/* Cho người dùng nhập vào 3 số nguyên. Viết chương trình xuất 3 số theo thứ tự tăng dần
    input: tạo 3 input để user nhập value 
    progress: lấy value gán cho 3 biến. điều kiện > 0
              Nếu 3 value = nhau thì xuất ra k giá trị bằng là == nhau 
              Nếu không k bằng dùng if else để lấy được từng vị trị trí lớn nhất hoặc nhỏ nhất.  
              So sánh 2 vị trí còn lại nếu bằng nhau thì xuất sau số bé nhất còn khác nhau thì xuất theo thứ tự từ bé đến lớn
    ouput: dùng innerHTML xuất ra ketQua nhận được.    
*/

document.getElementById('click__bai1').onclick = function() {
    var so1 = document.getElementById('so1').value*1;
    var so2 = document.getElementById('so2').value*1;
    var so3 = document.getElementById('so3').value*1;
    if(so1 > 0 && so2 > 0 && so3 > 0) {
        if(so1 === so2 && so2 === so3) {
            document.getElementById('result__bai1').innerHTML = `${so1} = ${so2} = ${so3}`;
        } else if (so1 > so2 && so1 > so3){
            if(so2 > so3 || so2 === so3) {
                document.getElementById('result__bai1').innerHTML = `${so3} ${so2} ${so1}`;
            } else {
                document.getElementById('result__bai1').innerHTML = `${so2} ${so3}  ${so1}`;
            }
        } else if (so2 > so3 && so2 > so1) {
            if(so1 > so3 || so1 === so3) {
                document.getElementById('result__bai1').innerHTML = ` ${so3} ${so1}  ${so2}`;
            } else {
                document.getElementById('result__bai1').innerHTML = ` ${so1}  ${so3}  ${so2}`;
            }
        } else {
            if (so1 > so2 || so1 === so2) {
                document.getElementById('result__bai1').innerHTML = `${so2} ${so1}  ${so3} ` ; 
            } else {
                document.getElementById('result__bai1').innerHTML = ` ${so1} ${so2}  ${so3} ` ; 
            }
        }
    } else {
         alert("Xin mời bài nhập vào số nguyên lớn hơn 0")
    }
}

/**
 * Bài 2: Viết chương trình “Chào hỏi” các thành viên trong gia đình với các đặc điểm. Đầu tiên máy sẽ
input: user nhập vào input
progress: lấy value user nhập so sánh với điều kiện 
          - dùng switch case sosanh
output: xuất ra câu chào tương ứng với điều kiện
 */

document.getElementById('click__bai2').onclick = function() {
    var who = document.getElementById('Who').value;
    switch(who) {
        case 'ban':{
            document.getElementById('result__bai2').innerHTML = `hello brooo !!`
        }break;
        case 'bo': {
            document.getElementById('result__bai2').innerHTML = `hello daddd !!`
        } break;
        case 'me': {
            document.getElementById('result__bai2').innerHTML = `hello momm !!`
        } break;
        case 'ny': {
            document.getElementById('result__bai2').innerHTML = `hello baeee!!`
        } break;
    } 
}


/**
 * Cho 3 số nguyên. Viết chương trình xuất ra có bao nhiêu số lẻ và bao nhiêu số chẵn.
input: láy giá trị user nhập vào từ input
progress: dùng if else để so sánh điều tiện
    + lớn > 0 
    + so sánh từng số xem có phải số chẵn hay k 
output: xuất ra xem có bnh số chắn bnh số lẻ
 */

document.getElementById('click__bai3').onclick = function() {
    var num1 = document.getElementById('so1_bai3').value*1;
    var num2 = document.getElementById('so2_bai3').value*1;
    var num3 = document.getElementById('so3_bai3').value*1;
    if(num1>0 && num2>0 && num3>0) {
        if (num1%2==0 && so2%2==0 && so3%2==0) {
            document.getElementById('result__bai3').innerHTML = `Có 3 số chẵn`
        } else if (num1%2==0 && num2%2==0 || num1%2==0 && num3%2==0 || num2%2==0 && num3%2==0) {
            document.getElementById('result__bai3').innerHTML = `Có 2 số chẵn và 1 số lẻ`
        } else if (num1%2==0 || num2%2==0 || num3%2==0) {
            document.getElementById('result__bai3').innerHTML = `Có 1 số chẵn và 2 số lẻ`
        } else {
            document.getElementById('result__bai3').innerHTML = `Có 3 số lẻ`
        }
    } else {
        alert("Nhập số lớn hơn 0");
    }
}

/**
 * Bài 4: Viết chương trình nhập vào 3 cánh của tam giác và cho biết đó là tam giác gì.
 * input: lấy value của user nhập vào.
 * progress: dùng if else xử lý
 *  + value > 0.
 *  + 3 cạnh bằng nhau => tam giác đều
 *  + 2 cạnh bằng nhau => tam giác cân
 *  + bình phương 1 cạnh bằng tổng bình phương 2 cạnh còn lại => tan giác vuông
 *  + 2 cạnh bằng nhau && tổng bình phương của 2 cạnh == bình phương 1 cạnh => tam giác vuông cân
 *  + Tam giác thường 
 * output: Xuất ra loại tam giác tương ứng với value mà user nhập vào
 */

document.getElementById('click__bai4').onclick = function() {
    var canh1 = document.getElementById('canh1').value*1;
    var canh2 = document.getElementById('canh2').value*1;
    var canh3 = document.getElementById('canh3').value*1;
    if(canh1>0 && canh2>0 && canh3>0) {
        if(canh1 === canh2 && canh2 === canh3) {
            document.getElementById('result__bai4').innerHTML = `Tam Giác Đều`
        } else if(canh1 === canh2 || canh1 === canh3 || canh2 === canh3) {
            if(canh1*canh1== canh2*canh2+canh3*canh3 || canh2*canh2== canh1*canh1+canh3*canh3 ||canh3*canh3== canh2*canh2+canh1*canh1) {
                document.getElementById('result__bai4').innerHTML = `Tam Giác Vuông Cân`  
            } else {
                document.getElementById('result__bai4').innerHTML = `Tam Giác Cân`  
            }
        } else if(canh1*canh1== canh2*canh2+canh3*canh3 || canh2*canh2== canh1*canh1+canh3*canh3 ||canh3*canh3== canh2*canh2+canh1*canh1) {
            document.getElementById('result__bai4').innerHTML = `Tam Giác Vuông`  
        } else {
            document.getElementById('result__bai4').innerHTML = `Tam Giác Thường`  
        }
    }else {
        alert('Nhập vào giá trị lớn hơn 0');
    }
}